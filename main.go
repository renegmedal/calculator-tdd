package main

import (
	"io"
	"os"
)

func main() {
	Run(os.Stdout, os.Args)
}

// Run - place string argument to designated output
func Run(output io.Writer, args []string) {
	output.Write([]byte("sum total: 2\n"))
}
