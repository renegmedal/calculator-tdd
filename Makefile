PROJECT-NAME := "calculator-tdd"
DIRS?=$$(go list ./... | grep -v 'vendor')
PKG := "github.com/renegmedal/$(PROJECT-NAME)"

test:
	@go test -v -i $(DIRS) || exit 1
	@echo $(DIRS) | xargs -t -n4 go test -v 

.PHONY: test 

build:
	@go build -i -v $(PKG)