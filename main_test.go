package main

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/pjbgf/go-test/should"
)

func TestMain(t *testing.T) {
	assertThat := func(assumption, command, expectedOutput string) {
		should := should.New(t)
		tmpfile, err := ioutil.TempFile("", "calc-fake-stdout.*")
		if err != nil {
			t.Fail()
		}
		defer os.Remove(tmpfile.Name())

		os.Stdout = tmpfile
		os.Args = strings.Split(command, " ")

		main()

		output, err := ioutil.ReadFile(tmpfile.Name())
		actualOutput := string(output)

		should.BeEqual(expectedOutput, actualOutput, assumption)
	}

	assertThat("should sum 1+1 and return 2", "calc 1 + 1", "sum total: 2\n")
}
